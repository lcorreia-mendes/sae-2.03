# Playbook add_user_grp

```bash
---
- name: Ajouter un groupe sur le serveur
  hosts: servers
  become: true
  vars:
    nom_grp : "{{ nom_goupe }}"
    nom_user : “{{ nom_utilisateur }}”
 
  tasks:
    - name: Créer un groupe
      group:
        name: “{{ nom_grp }}”
        state: present

- name: Créer un utilisateur
      user:
        name: "{{ nom_user }}"
        state: present
        groups: "{{ nom_grp }}"

- name: Définir le mot de passe pour l'utilisateur
      user:
        name: "{{ nom_user }}"
        password: "{{ 'mdp_chiffré' | password_hash('sha512') }}"

```

## Explication ligne par ligne : 

Les premières lignes sont identiques au premier Playbook.

### Ligne 7 : 
```bash 
nom_user: "{{ nom_utilisateur }}"
```
Définit une variable nom_user qui est définie avec la valeur de la variable nom_utilisateur.


### Ligne 9-13 : 
```bash 
tasks:
    - name: Créer un groupe
      group:
        name: “{{ nom_grp }}”
        state: present
```
Ici le code est identique au playbook 1, en effet c'est la partie de création du groupe.
<br>

### Ligne 15 : 
```bash 
- name: Créer un utilisateur
```
Définit une autre tâche nommée "Créer un utilisateur".
<br>

### Ligne 16-20 : 
```bash 
user:
        name: "{{ nom_user }}"
        state: present
        groups: "{{ nom_grp }}"
```
Utilise le module user pour créer un utilisateur avec le nom spécifié dans la variable nom_user. La valeur state: present indique que l'utilisateur doit être présent (c'est-à-dire créé s'il n'existe pas). L'utilisateur est ajouté au groupe spécifié dans la variable nom_grp.
<br>

### Ligne 21 : 
```bash 
- name: Définir le mot de passe pour l'utilisateur
```
Définit une autre tâche nommée "Définir le mot de passe pour l'utilisateur".
<br>

### Ligne 22-24 : 
```bash 
 user:
        name: "{{ nom_user }}"
        password: "{{ 'mdp_chiffré' | password_hash('sha512') }}"
```
Utilise à nouveau le module user pour définir le mot de passe de l'utilisateur spécifié dans la variable nom_user. Le mot de passe est défini à l'aide de la fonction password_hash qui chiffre le mot de passe en utilisant l'algorithme SHA-512


## Explication global de l'utilité du script :

Ce script permet à l’appelle de la commande de saisir un utilisateur un groupe, ainsi qu’un mot de passe pour l’utilisateur. A l’execution de la commande le groupe va se créer, ensuite l’utilisateur va se créer et être associé au mot de passe, puis va être directement intégrer au groupe créer.



